File 1: psma_16_genes_2_classes.ipynb
 This file contains python code for classification based on gleason considering 7 and 9 score (whose sample size more than others) also considering 16 selected genes given by Sumith
 Accuracy:81.76% 

file 2: psma_16_genes_all_classes.ipynb
  This file contains python code for classification based on gleason considering all 6 to 10 score (with 6, 7 and (8,9,10) merged) also considering 16 selected genes given by Sumith
 Accuracy:68.21% 
 
The above accuracy is little lesser than the accuracy obtained by the program considering 7 and 9 only (with all the genes in place) which is shared by Anand.